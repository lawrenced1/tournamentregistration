<?php if(stristr(__FILE__, $_SERVER['PHP_SELF'])) die(header('HTTP/1.0 404 Not Found'));?>
<?php

date_default_timezone_set('America/Chicago');

//Setting time limit
set_time_limit(300);

//Shortened from DEVELOPMENT_ENVIRONEMENT to DE for easier coding
define('DEVELOPMENT_ENVIRONMENT', TRUE);
define('ERROR_LOG_FILE', ROOT.DS.'tmp'.DS.'logs'.DS.'error.log');
define( 'CSRF_ENABLED', TRUE );

if ( ! file_exists(ERROR_LOG_FILE) ) 
{
	/** 
	 * Lets check and see if we can create the directory structure we need
	 */
	//exit ( exec( 'whoami' ) );
	if ( ! file_exists( ROOT.DS.'tmp') )
	{
		if ( ! mkdir( ROOT.DS.'tmp'.DS.'logs', 0775, TRUE ) )
		{
			exit("Unable to create necessary logging folders");
		}
	}
	elseif ( ! file_exists( ROOT.DS.'tmp'.DS.'logs' ) )
	{
		if ( ! mkdir( ROOT.DS.'tmp'.DS.'logs', 0775 ) )
		{
			exit( "Unable to create necessary logging folders" );
		}
	}
}

//i might want to move this back to the if and change the verbosity---we'll see
error_reporting(E_ALL | E_STRICT);
ini_set('log_errors', 'On');
ini_set('error_log', ERROR_LOG_FILE);

//Error Display Handling Logic
if (DEVELOPMENT_ENVIRONMENT) 
{
	ini_set('display_errors','On');
	Debug::DebugLevel(1);
	Debug::ToScreen();
} 
else 
{
	ini_set('display_errors','Off');
	Debug::DebugLevel(4);
	Debug::ToLog();
}

//Convert errors to Exceptions
//----------------------------
//Any Error that is triggered will be converted into an exception than can
//then bubble up the exception stack and be handled by the application
//this works because autoloader.php has been called
if ( !DEVELOPMENT_ENVIRONMENT )
{
	set_error_handler(array('ErrorHandler', 'captureError'));
	set_exception_handler(array('ErrorHandler', 'captureException'));
	register_shutdown_function(array('ErrorHandler', 'captureShutdown'));
}


/** Check for Magic Quotes and remove them **/

function stripSlashesDeep($value) 
{
	$value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
	return $value;
}

if ( get_magic_quotes_gpc() ) 
{
	$_GET    = stripSlashesDeep($_GET   );
	$_POST   = stripSlashesDeep($_POST  );
	$_COOKIE = stripSlashesDeep($_COOKIE);
}

/** Check register globals and remove them **/
if (ini_get('register_globals')) {
	$array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
	foreach ($array as $value) {
		foreach ($GLOBALS[$value] as $key => $var) {
			if ($var === $GLOBALS[$key]) {
				unset($GLOBALS[$key]);
			}
		}
	}
}
