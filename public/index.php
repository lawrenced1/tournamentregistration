<?php
/**
 * index.php The index file in this application framework.
 * 
 * index.php Sets up the location of directories and creates constants to be used through out the framework for link creation and to help the application sit in directories offset from the root directory of the server.
 *
 * @package nuFramework
 * @author Don Lawrence <lawrenced1@gmail.com>
 * @version 2.0
 * 
 */

/**
 * @const DS Shorthand code for the built-in DIRECTORY_SEPARATOR to save on typing. This allows the application to be written unaware of the underlying
 * operating system.
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * @const ROOT Used to define the root folder of the application and allows the application to live in any directory on the server
 */
define('ROOT', dirname(dirname(__FILE__)));

/**
 * @const APP_ROOT Application path definition used in creating links in the framework. Allows the app to live side-by-side other routing applications like wordpress.
 */
define('APP_ROOT', str_replace('/public/index.php', '', str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME'])));

/**
 * @const REGISTRY_FILE Registry Location. Set here for access by multiple modules
 */
define( 'REGISTRY_FILE', ROOT.DS.'config'.DS.'registry.php' );

/**
 * @const APP_NAME Constant used to create dynamic links without knowing the name of the controller we are using
 *
 * might drop this one, not sure if the developer shouldn't know what his controller is.
 */
//This defines the application name for dynamic link creations without knowing the controller we are using
$strAppName = str_replace(APP_ROOT, '', $_SERVER['REQUEST_URI']);
$aryAppName = explode('/', $strAppName);
$strAppName = empty($aryAppName[0]) ? $aryAppName[1] : $aryAppName[0];
if (strpos($strAppName, '?'))
{
	$aryAppName = explode('?', $strAppName);
	$strAppName = $aryAppName[0];
} 
define('APP_NAME', $strAppName);

/**
 * @const APP_PATH Combined constant used for URI creation in the framework.
 */
define('APP_PATH', APP_ROOT . '/' . APP_NAME);

/**
 * @var  url Variable for storing the whole URL originally passed to the server before being rewritten to our framework.
 *
 * Allows the application to breakdown the URI into its specific components {controller/view/var/var/...} and done before we cleanse the global variables to prevent contamination from any 3rd party modules.
 */
$url = isset($_GET['url']) ? $_GET['url'] : NULL;

/**
 * If you need to set a specific route to start, here is the place to set a default
 */
//if ($url == NULL) $url = "{controller}/{view}";

/**
 * Call the bootstrap to begin assembling the framework before calling the main function.
 */
require_once (ROOT . DS . 'nucore' . DS . 'bootstrap.php');